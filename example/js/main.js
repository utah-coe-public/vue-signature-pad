Vue.component('vue-signature-pad', VueSignaturePad);

  var vue = new Vue({
    el: '.vue.main-app',
    data: {},
    computed: {},
    methods: {
      //methodName: function(){},
      clear: function() {
        this.$refs.signaturePad.clearSignature();
      },
      undo: function(){
        this.$refs.signaturePad.undoSignature();        
      },
      save: function(){
        console.log(this.$refs.signaturePad.saveSignature());
      },
      stateChange: function(new_val){
        console.log(VueSignaturePad.PAD_STATES.EMPTY !== new_val);        
      }

    },
    mounted: function() {

    },
    watch: {
      //data var: function(){},
    },
  })
