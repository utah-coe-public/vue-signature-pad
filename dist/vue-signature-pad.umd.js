(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
    typeof define === 'function' && define.amd ? define(factory) :
    (global.VueSignaturePad = factory());
}(this, (function () { 'use strict';

    /*!
     * Signature Pad v3.0.0-beta.3 | https://github.com/szimek/signature_pad
     * (c) 2018 Szymon Nowak | Released under the MIT license
     */

    class Point {
        constructor(x, y, time) {
            this.x = x;
            this.y = y;
            this.time = time || Date.now();
        }
        distanceTo(start) {
            return Math.sqrt(Math.pow(this.x - start.x, 2) + Math.pow(this.y - start.y, 2));
        }
        equals(other) {
            return this.x === other.x && this.y === other.y && this.time === other.time;
        }
        velocityFrom(start) {
            return (this.time !== start.time) ? this.distanceTo(start) / (this.time - start.time) : 0;
        }
    }

    class Bezier {
        constructor(startPoint, control2, control1, endPoint, startWidth, endWidth) {
            this.startPoint = startPoint;
            this.control2 = control2;
            this.control1 = control1;
            this.endPoint = endPoint;
            this.startWidth = startWidth;
            this.endWidth = endWidth;
        }
        static fromPoints(points, widths) {
            const c2 = this.calculateControlPoints(points[0], points[1], points[2]).c2;
            const c3 = this.calculateControlPoints(points[1], points[2], points[3]).c1;
            return new Bezier(points[1], c2, c3, points[2], widths.start, widths.end);
        }
        static calculateControlPoints(s1, s2, s3) {
            const dx1 = s1.x - s2.x;
            const dy1 = s1.y - s2.y;
            const dx2 = s2.x - s3.x;
            const dy2 = s2.y - s3.y;
            const m1 = { x: (s1.x + s2.x) / 2.0, y: (s1.y + s2.y) / 2.0 };
            const m2 = { x: (s2.x + s3.x) / 2.0, y: (s2.y + s3.y) / 2.0 };
            const l1 = Math.sqrt((dx1 * dx1) + (dy1 * dy1));
            const l2 = Math.sqrt((dx2 * dx2) + (dy2 * dy2));
            const dxm = (m1.x - m2.x);
            const dym = (m1.y - m2.y);
            const k = l2 / (l1 + l2);
            const cm = { x: m2.x + (dxm * k), y: m2.y + (dym * k) };
            const tx = s2.x - cm.x;
            const ty = s2.y - cm.y;
            return {
                c1: new Point(m1.x + tx, m1.y + ty),
                c2: new Point(m2.x + tx, m2.y + ty),
            };
        }
        length() {
            const steps = 10;
            let length = 0;
            let px;
            let py;
            for (let i = 0; i <= steps; i += 1) {
                const t = i / steps;
                const cx = this.point(t, this.startPoint.x, this.control1.x, this.control2.x, this.endPoint.x);
                const cy = this.point(t, this.startPoint.y, this.control1.y, this.control2.y, this.endPoint.y);
                if (i > 0) {
                    const xdiff = cx - px;
                    const ydiff = cy - py;
                    length += Math.sqrt((xdiff * xdiff) + (ydiff * ydiff));
                }
                px = cx;
                py = cy;
            }
            return length;
        }
        point(t, start, c1, c2, end) {
            return (start * (1.0 - t) * (1.0 - t) * (1.0 - t))
                + (3.0 * c1 * (1.0 - t) * (1.0 - t) * t)
                + (3.0 * c2 * (1.0 - t) * t * t)
                + (end * t * t * t);
        }
    }

    function throttle(fn, wait = 250) {
        let previous = 0;
        let timeout = null;
        let result;
        let storedContext;
        let storedArgs;
        const later = () => {
            previous = Date.now();
            timeout = null;
            result = fn.apply(storedContext, storedArgs);
            if (!timeout) {
                storedContext = null;
                storedArgs = [];
            }
        };
        return function (...args) {
            const now = Date.now();
            const remaining = wait - (now - previous);
            storedContext = this;
            storedArgs = args;
            if (remaining <= 0 || remaining > wait) {
                if (timeout) {
                    clearTimeout(timeout);
                    timeout = null;
                }
                previous = now;
                result = fn.apply(storedContext, storedArgs);
                if (!timeout) {
                    storedContext = null;
                    storedArgs = [];
                }
            }
            else if (!timeout) {
                timeout = window.setTimeout(later, remaining);
            }
            return result;
        };
    }

    class SignaturePad {
        constructor(canvas, options = {}) {
            this.canvas = canvas;
            this.options = options;
            this._handleMouseDown = (event) => {
                if (event.which === 1) {
                    this._mouseButtonDown = true;
                    this._strokeBegin(event);
                }
            };
            this._handleMouseMove = (event) => {
                if (this._mouseButtonDown) {
                    this._strokeMoveUpdate(event);
                }
            };
            this._handleMouseUp = (event) => {
                if (event.which === 1 && this._mouseButtonDown) {
                    this._mouseButtonDown = false;
                    this._strokeEnd(event);
                }
            };
            this._handleTouchStart = (event) => {
                event.preventDefault();
                if (event.targetTouches.length === 1) {
                    const touch = event.changedTouches[0];
                    this._strokeBegin(touch);
                }
            };
            this._handleTouchMove = (event) => {
                event.preventDefault();
                const touch = event.targetTouches[0];
                this._strokeMoveUpdate(touch);
            };
            this._handleTouchEnd = (event) => {
                const wasCanvasTouched = event.target === this.canvas;
                if (wasCanvasTouched) {
                    event.preventDefault();
                    const touch = event.changedTouches[0];
                    this._strokeEnd(touch);
                }
            };
            this.velocityFilterWeight = options.velocityFilterWeight || 0.7;
            this.minWidth = options.minWidth || 0.5;
            this.maxWidth = options.maxWidth || 2.5;
            this.throttle = ('throttle' in options ? options.throttle : 16);
            this.minDistance = ('minDistance' in options ? options.minDistance : 5);
            if (this.throttle) {
                this._strokeMoveUpdate = throttle(SignaturePad.prototype._strokeUpdate, this.throttle);
            }
            else {
                this._strokeMoveUpdate = SignaturePad.prototype._strokeUpdate;
            }
            this.dotSize = options.dotSize || function () {
                return (this.minWidth + this.maxWidth) / 2;
            };
            this.penColor = options.penColor || 'black';
            this.backgroundColor = options.backgroundColor || 'rgba(0,0,0,0)';
            this.onBegin = options.onBegin;
            this.onEnd = options.onEnd;
            this._ctx = canvas.getContext('2d');
            this.clear();
            this.on();
        }
        clear() {
            const ctx = this._ctx;
            const canvas = this.canvas;
            ctx.fillStyle = this.backgroundColor;
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            this._data = [];
            this._reset();
            this._isEmpty = true;
        }
        fromDataURL(dataUrl, options = {}, callback) {
            const image = new Image();
            const ratio = options.ratio || window.devicePixelRatio || 1;
            const width = options.width || (this.canvas.width / ratio);
            const height = options.height || (this.canvas.height / ratio);
            this._reset();
            image.onload = () => {
                this._ctx.drawImage(image, 0, 0, width, height);
                if (callback) {
                    callback();
                }
            };
            image.onerror = (error) => {
                if (callback) {
                    callback(error);
                }
            };
            image.src = dataUrl;
            this._isEmpty = false;
        }
        toDataURL(type = 'image/png', encoderOptions) {
            switch (type) {
                case 'image/svg+xml':
                    return this._toSVG();
                default:
                    return this.canvas.toDataURL(type, encoderOptions);
            }
        }
        on() {
            this.canvas.style.touchAction = 'none';
            this.canvas.style.msTouchAction = 'none';
            if (window.PointerEvent) {
                this._handlePointerEvents();
            }
            else {
                this._handleMouseEvents();
                if ('ontouchstart' in window) {
                    this._handleTouchEvents();
                }
            }
        }
        off() {
            this.canvas.style.touchAction = 'auto';
            this.canvas.style.msTouchAction = 'auto';
            this.canvas.removeEventListener('pointerdown', this._handleMouseDown);
            this.canvas.removeEventListener('pointermove', this._handleMouseMove);
            document.removeEventListener('pointerup', this._handleMouseUp);
            this.canvas.removeEventListener('mousedown', this._handleMouseDown);
            this.canvas.removeEventListener('mousemove', this._handleMouseMove);
            document.removeEventListener('mouseup', this._handleMouseUp);
            this.canvas.removeEventListener('touchstart', this._handleTouchStart);
            this.canvas.removeEventListener('touchmove', this._handleTouchMove);
            this.canvas.removeEventListener('touchend', this._handleTouchEnd);
        }
        isEmpty() {
            return this._isEmpty;
        }
        fromData(pointGroups) {
            this.clear();
            this._fromData(pointGroups, ({ color, curve }) => this._drawCurve({ color, curve }), ({ color, point }) => this._drawDot({ color, point }));
            this._data = pointGroups;
        }
        toData() {
            return this._data;
        }
        _strokeBegin(event) {
            const newPointGroup = {
                color: this.penColor,
                points: [],
            };
            this._data.push(newPointGroup);
            this._reset();
            this._strokeUpdate(event);
            if (typeof this.onBegin === 'function') {
                this.onBegin(event);
            }
        }
        _strokeUpdate(event) {
            const x = event.clientX;
            const y = event.clientY;
            const point = this._createPoint(x, y);
            const lastPointGroup = this._data[this._data.length - 1];
            const lastPoints = lastPointGroup.points;
            const lastPoint = lastPoints.length > 0 && lastPoints[lastPoints.length - 1];
            const isLastPointTooClose = lastPoint ? point.distanceTo(lastPoint) <= this.minDistance : false;
            const color = lastPointGroup.color;
            if (!lastPoint || !(lastPoint && isLastPointTooClose)) {
                const curve = this._addPoint(point);
                if (!lastPoint) {
                    this._drawDot({ color, point });
                }
                else if (curve) {
                    this._drawCurve({ color, curve });
                }
                lastPoints.push({
                    time: point.time,
                    x: point.x,
                    y: point.y,
                });
            }
        }
        _strokeEnd(event) {
            this._strokeUpdate(event);
            if (typeof this.onEnd === 'function') {
                this.onEnd(event);
            }
        }
        _handlePointerEvents() {
            this._mouseButtonDown = false;
            this.canvas.addEventListener('pointerdown', this._handleMouseDown);
            this.canvas.addEventListener('pointermove', this._handleMouseMove);
            document.addEventListener('pointerup', this._handleMouseUp);
        }
        _handleMouseEvents() {
            this._mouseButtonDown = false;
            this.canvas.addEventListener('mousedown', this._handleMouseDown);
            this.canvas.addEventListener('mousemove', this._handleMouseMove);
            document.addEventListener('mouseup', this._handleMouseUp);
        }
        _handleTouchEvents() {
            this.canvas.addEventListener('touchstart', this._handleTouchStart);
            this.canvas.addEventListener('touchmove', this._handleTouchMove);
            this.canvas.addEventListener('touchend', this._handleTouchEnd);
        }
        _reset() {
            this._lastPoints = [];
            this._lastVelocity = 0;
            this._lastWidth = (this.minWidth + this.maxWidth) / 2;
            this._ctx.fillStyle = this.penColor;
        }
        _createPoint(x, y) {
            const rect = this.canvas.getBoundingClientRect();
            return new Point(x - rect.left, y - rect.top, new Date().getTime());
        }
        _addPoint(point) {
            const { _lastPoints } = this;
            _lastPoints.push(point);
            if (_lastPoints.length > 2) {
                if (_lastPoints.length === 3) {
                    _lastPoints.unshift(_lastPoints[0]);
                }
                const widths = this._calculateCurveWidths(_lastPoints[1], _lastPoints[2]);
                const curve = Bezier.fromPoints(_lastPoints, widths);
                _lastPoints.shift();
                return curve;
            }
            return null;
        }
        _calculateCurveWidths(startPoint, endPoint) {
            const velocity = (this.velocityFilterWeight * endPoint.velocityFrom(startPoint))
                + ((1 - this.velocityFilterWeight) * this._lastVelocity);
            const newWidth = this._strokeWidth(velocity);
            const widths = {
                end: newWidth,
                start: this._lastWidth,
            };
            this._lastVelocity = velocity;
            this._lastWidth = newWidth;
            return widths;
        }
        _strokeWidth(velocity) {
            return Math.max(this.maxWidth / (velocity + 1), this.minWidth);
        }
        _drawCurveSegment(x, y, width) {
            const ctx = this._ctx;
            ctx.moveTo(x, y);
            ctx.arc(x, y, width, 0, 2 * Math.PI, false);
            this._isEmpty = false;
        }
        _drawCurve({ color, curve }) {
            const ctx = this._ctx;
            const widthDelta = curve.endWidth - curve.startWidth;
            const drawSteps = Math.floor(curve.length()) * 2;
            ctx.beginPath();
            ctx.fillStyle = color;
            for (let i = 0; i < drawSteps; i += 1) {
                const t = i / drawSteps;
                const tt = t * t;
                const ttt = tt * t;
                const u = 1 - t;
                const uu = u * u;
                const uuu = uu * u;
                let x = uuu * curve.startPoint.x;
                x += 3 * uu * t * curve.control1.x;
                x += 3 * u * tt * curve.control2.x;
                x += ttt * curve.endPoint.x;
                let y = uuu * curve.startPoint.y;
                y += 3 * uu * t * curve.control1.y;
                y += 3 * u * tt * curve.control2.y;
                y += ttt * curve.endPoint.y;
                const width = curve.startWidth + (ttt * widthDelta);
                this._drawCurveSegment(x, y, width);
            }
            ctx.closePath();
            ctx.fill();
        }
        _drawDot({ color, point }) {
            const ctx = this._ctx;
            const width = typeof this.dotSize === 'function' ? this.dotSize() : this.dotSize;
            ctx.beginPath();
            this._drawCurveSegment(point.x, point.y, width);
            ctx.closePath();
            ctx.fillStyle = color;
            ctx.fill();
        }
        _fromData(pointGroups, drawCurve, drawDot) {
            for (const group of pointGroups) {
                const { color, points } = group;
                if (points.length > 1) {
                    for (let j = 0; j < points.length; j += 1) {
                        const basicPoint = points[j];
                        const point = new Point(basicPoint.x, basicPoint.y, basicPoint.time);
                        this.penColor = color;
                        if (j === 0) {
                            this._reset();
                        }
                        const curve = this._addPoint(point);
                        if (curve) {
                            drawCurve({ color, curve });
                        }
                    }
                }
                else {
                    this._reset();
                    drawDot({
                        color,
                        point: points[0],
                    });
                }
            }
        }
        _toSVG() {
            const pointGroups = this._data;
            const ratio = Math.max(window.devicePixelRatio || 1, 1);
            const minX = 0;
            const minY = 0;
            const maxX = this.canvas.width / ratio;
            const maxY = this.canvas.height / ratio;
            const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
            svg.setAttribute('width', this.canvas.width.toString());
            svg.setAttribute('height', this.canvas.height.toString());
            this._fromData(pointGroups, ({ color, curve }) => {
                const path = document.createElement('path');
                if (!isNaN(curve.control1.x) &&
                    !isNaN(curve.control1.y) &&
                    !isNaN(curve.control2.x) &&
                    !isNaN(curve.control2.y)) {
                    const attr = `M ${curve.startPoint.x.toFixed(3)},${curve.startPoint.y.toFixed(3)} `
                        + `C ${curve.control1.x.toFixed(3)},${curve.control1.y.toFixed(3)} `
                        + `${curve.control2.x.toFixed(3)},${curve.control2.y.toFixed(3)} `
                        + `${curve.endPoint.x.toFixed(3)},${curve.endPoint.y.toFixed(3)}`;
                    path.setAttribute('d', attr);
                    path.setAttribute('stroke-width', (curve.endWidth * 2.25).toFixed(3));
                    path.setAttribute('stroke', color);
                    path.setAttribute('fill', 'none');
                    path.setAttribute('stroke-linecap', 'round');
                    svg.appendChild(path);
                }
            }, ({ color, point }) => {
                const circle = document.createElement('circle');
                const dotSize = typeof this.dotSize === 'function' ? this.dotSize() : this.dotSize;
                circle.setAttribute('r', dotSize.toString());
                circle.setAttribute('cx', point.x.toString());
                circle.setAttribute('cy', point.y.toString());
                circle.setAttribute('fill', color);
                svg.appendChild(circle);
            });
            const prefix = 'data:image/svg+xml;base64,';
            const header = '<svg'
                + ' xmlns="http://www.w3.org/2000/svg"'
                + ' xmlns:xlink="http://www.w3.org/1999/xlink"'
                + ` viewBox="${minX} ${minY} ${maxX} ${maxY}"`
                + ` width="${maxX}"`
                + ` height="${maxY}"`
                + '>';
            let body = svg.innerHTML;
            if (body === undefined) {
                const dummy = document.createElement('dummy');
                const nodes = svg.childNodes;
                dummy.innerHTML = '';
                for (let i = 0; i < nodes.length; i += 1) {
                    dummy.appendChild(nodes[i].cloneNode(true));
                }
                body = dummy.innerHTML;
            }
            const footer = '</svg>';
            const data = header + body + footer;
            return prefix + btoa(data);
        }
    }

    // Defaults
    var defaultOptions = {
    	format: 'image/png',
    	quality: 0.92,
    	width: undefined,
    	height: undefined,
    	Canvas: undefined
    };

    // Return Promise
    var mergeImages = function (sources, options) {
    	if ( sources === void 0 ) sources = [];
    	if ( options === void 0 ) options = {};

    	return new Promise(function (resolve) {
    	options = Object.assign({}, defaultOptions, options);

    	// Setup browser/Node.js specific variables
    	var canvas = options.Canvas ? new options.Canvas() : window.document.createElement('canvas');
    	var Image = options.Canvas ? options.Canvas.Image : window.Image;
    	if (options.Canvas) {
    		options.quality *= 100;
    	}

    	// Load sources
    	var images = sources.map(function (source) { return new Promise(function (resolve, reject) {
    		// Convert sources to objects
    		if (source.constructor.name !== 'Object') {
    			source = { src: source };
    		}

    		// Resolve source and img when loaded
    		var img = new Image();
    		img.onerror = function () { return reject(new Error('Couldn\'t load image')); };
    		img.onload = function () { return resolve(Object.assign({}, source, { img: img })); };
    		img.src = source.src;
    	}); });

    	// Get canvas context
    	var ctx = canvas.getContext('2d');

    	// When sources have loaded
    	resolve(Promise.all(images)
    		.then(function (images) {
    			// Set canvas dimensions
    			var getSize = function (dim) { return options[dim] || Math.max.apply(Math, images.map(function (image) { return image.img[dim]; })); };
    			canvas.width = getSize('width');
    			canvas.height = getSize('height');

    			// Draw images to canvas
    			images.forEach(function (image) {
    				ctx.globalAlpha = image.opacity ? image.opacity : 1;
    				return ctx.drawImage(image.img, image.x || 0, image.y || 0);
    			});

    			if (options.Canvas && options.format === 'image/jpeg') {
    				// Resolve data URI for node-canvas jpeg async
    				return new Promise(function (resolve) {
    					canvas.toDataURL(options.format, {
    						quality: options.quality,
    						progressive: false
    					}, function (err, jpeg) {
    						if (err) {
    							throw err;
    						}
    						resolve(jpeg);
    					});
    				});
    			}

    			// Resolve all other data URIs sync
    			return canvas.toDataURL(options.format, options.quality);
    		}));
    });
    };

    const SAVE_TYPE = ['image/png', 'image/jpeg', 'image/svg+xml'];

    const checkSaveType = type => SAVE_TYPE.includes(type);

    const DEFAULT_OPTIONS = {
      dotSize: (0.5 + 2.5) / 2,
      minWidth: 0.5,
      maxWidth: 2.5,
      throttle: 16,
      minDistance: 5,
      backgroundColor: 'rgba(0,0,0,0)',
      penColor: 'black',
      velocityFilterWeight: 0.7,
      onBegin: () => {},
      onEnd: () => {}
    };

    const convert2NonReactive = observerValue =>
      JSON.parse(JSON.stringify(observerValue));

    const TRANSPARENT_PNG = {
      src:
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=',
      x: 0,
      y: 0
    };

    const PAD_STATES = {EMPTY:'empty', DRAWN:'drawn'};

    //

    var script = {
        PAD_STATES: PAD_STATES,
        name: 'VueSignaturePad',
        
        props: {
            width: {
                type: String,
                default: '100%'
            },
            height: {
                type: String,
                default: '100%'
            },
            customStyle: {
                type: Object
            },
            saveType: {
                type: String,
                default: 'image/png'
            },
            options: {
                type: Object,
                default: () => ({})
            },
            background_image: {
                type: String,
                default: ''
            },
            images: {
                type: Array,
                default: () => []
            }
        },
        data: () => ({
            signaturePad: {},
            cacheImages: [],
            signatureData: TRANSPARENT_PNG,
            onResizeHandler: null,
            pad_state: PAD_STATES.EMPTY
        }),
        mounted() {
            const { options } = this;
            const canvas = this.$refs.signaturePadCanvas;
            const signaturePad = new SignaturePad(canvas, {
                ...DEFAULT_OPTIONS,
                ...options,
                ...{ onEnd: e => { this.pad_state = PAD_STATES.DRAWN; } }
            });

            this.signaturePad = signaturePad;
            this.onResizeHandler = this.resizeCanvas.bind(this);
            window.addEventListener('resize', this.onResizeHandler, false);
            this.resizeCanvas();
        },
        beforeDestroy() {
            if (this.onResizeHandler) {
                window.removeEventListener('resize', this.onResizeHandler, false);
            }
        },
        methods: {
            resizeCanvas() {
                const canvas = this.$refs.signaturePadCanvas;
                const data = this.signaturePad.toData();
                var data_url = this.signaturePad.toDataURL(this.saveType);
                const ratio = Math.max(window.devicePixelRatio || 1, 1);
                canvas.width = canvas.offsetWidth * ratio;
                canvas.height = canvas.offsetHeight * ratio;
                canvas.getContext('2d').scale(ratio, ratio);
                this.signaturePad.clear();

                this.signatureData = TRANSPARENT_PNG;
                this.signaturePad.fromData(data);
            },
            saveSignature() {
                const { signaturePad, saveType } = this;
                const status = { isEmpty: false, data: undefined };

                if (!checkSaveType(saveType)) {
                    throw new Error('Image type is incorrect!');
                }

                if (signaturePad.isEmpty()) {
                    return {
                        ...status,
                        isEmpty: true
                    };
                } else {
                    this.signatureData = signaturePad.toDataURL(saveType);
                    return {
                        ...status,
                        data: this.signatureData,
                    };
                }
            },
            undoSignature() {
                const { signaturePad } = this;
                const record = signaturePad.toData();

                if (record) {
                    var undone_record = record.slice(0, -1);
                    if(undone_record.length < 1){
                        this.pad_state = PAD_STATES.EMPTY;
                    }
                    return signaturePad.fromData(undone_record);
                }
                else{
                    this.pad_state = PAD_STATES.EMPTY;
                }
            },
            mergeImageAndSignature(customSignature) {
                this.signatureData = customSignature;

                return mergeImages([
                    ...this.images,
                    ...this.cacheImages,
                    this.signatureData
                ]);
            },
            addImages(images = []) {
                this.cacheImages = [...this.cacheImages, ...images];
                return mergeImages([
                    ...this.images,
                    ...this.cacheImages,
                    this.signatureData
                ]);
            },
            fromDataURL(data) {
                return this.signaturePad.fromDataURL(data);
            },
            lockSignaturePad() {
                return this.signaturePad.off();
            },
            openSignaturePad() {
                return this.signaturePad.on();
            },
            isEmpty() {
                return this.signaturePad.isEmpty();
            },
            getPropImagesAndCacheImages() {
                return this.propsImagesAndCustomImages;
            },
            clearCacheImages() {
                this.cacheImages = [];

                return this.cacheImages;
            },
            clearSignature() {
                this.pad_state = PAD_STATES.EMPTY;
                return this.signaturePad.clear();
            }
        },
        computed: {
            propsImagesAndCustomImages() {
                const nonReactiveProrpImages = convert2NonReactive(this.images);
                const nonReactiveCachImages = convert2NonReactive(this.cacheImages);

                return [...nonReactiveProrpImages, ...nonReactiveCachImages];
            }
        },
        watch:{
            pad_state: function(new_val, old_val){
                this.$emit('state-change', new_val);
            }
        }
    };

    /* script */
                const __vue_script__ = script;
                
    /* template */
    var __vue_render__ = function() {
      var _vm = this;
      var _h = _vm.$createElement;
      var _c = _vm._self._c || _h;
      return _c("div", [
        _c(
          "div",
          {
            staticStyle: { "background-color": "white", border: "1px black solid" },
            style: {
              width: _vm.width,
              height: _vm.height,
              backgroundImage: "url(" + _vm.background_image + ")"
            },
            attrs: { id: "canvas-container" }
          },
          [
            _c("canvas", {
              ref: "signaturePadCanvas",
              attrs: { id: "signature-canvas" }
            })
          ]
        )
      ])
    };
    var __vue_staticRenderFns__ = [];
    __vue_render__._withStripped = true;

      /* style */
      const __vue_inject_styles__ = function (inject) {
        if (!inject) return
        inject("data-v-12e478a0_0", { source: "\n#canvas-container {}\n#signature-canvas {\n    width: 100%;\n    height: 100%;\n}\n", map: {"version":3,"sources":["/Users/matt.gauthier/Projects/university-of-utah/gitlab/utah-coe-public/vue-signature-pad/src/components/VueSignaturePad.vue"],"names":[],"mappings":";AA0LA,oBAAA;AAEA;IACA,YAAA;IACA,aAAA;CACA","file":"VueSignaturePad.vue","sourcesContent":["<template>\n    <div>\n        <div id=\"canvas-container\" style=\"background-color:white; border: 1px black solid;\" v-bind:style=\"{width: width, height: height, backgroundImage: 'url(' + background_image + ')'}\">\n            <canvas id=\"signature-canvas\" ref=\"signaturePadCanvas\">\n            </canvas>\n        </div>\n    </div>\n</template>\n<script>\nimport SignaturePad from 'signature_pad';\nimport mergeImages from 'merge-images';\nimport {\n    DEFAULT_OPTIONS,\n    TRANSPARENT_PNG,\n    checkSaveType,\n    convert2NonReactive,\n    PAD_STATES\n} from '../utils/index';\n\nexport default {\n    PAD_STATES: PAD_STATES,\n    name: 'VueSignaturePad',\n    \n    props: {\n        width: {\n            type: String,\n            default: '100%'\n        },\n        height: {\n            type: String,\n            default: '100%'\n        },\n        customStyle: {\n            type: Object\n        },\n        saveType: {\n            type: String,\n            default: 'image/png'\n        },\n        options: {\n            type: Object,\n            default: () => ({})\n        },\n        background_image: {\n            type: String,\n            default: ''\n        },\n        images: {\n            type: Array,\n            default: () => []\n        }\n    },\n    data: () => ({\n        signaturePad: {},\n        cacheImages: [],\n        signatureData: TRANSPARENT_PNG,\n        onResizeHandler: null,\n        pad_state: PAD_STATES.EMPTY\n    }),\n    mounted() {\n        const { options } = this;\n        const canvas = this.$refs.signaturePadCanvas;\n        const signaturePad = new SignaturePad(canvas, {\n            ...DEFAULT_OPTIONS,\n            ...options,\n            ...{ onEnd: e => { this.pad_state = PAD_STATES.DRAWN } }\n        });\n\n        this.signaturePad = signaturePad;\n        this.onResizeHandler = this.resizeCanvas.bind(this);\n        window.addEventListener('resize', this.onResizeHandler, false);\n        this.resizeCanvas();\n    },\n    beforeDestroy() {\n        if (this.onResizeHandler) {\n            window.removeEventListener('resize', this.onResizeHandler, false);\n        }\n    },\n    methods: {\n        resizeCanvas() {\n            const canvas = this.$refs.signaturePadCanvas;\n            const data = this.signaturePad.toData();\n            var data_url = this.signaturePad.toDataURL(this.saveType);\n            const ratio = Math.max(window.devicePixelRatio || 1, 1);\n            canvas.width = canvas.offsetWidth * ratio;\n            canvas.height = canvas.offsetHeight * ratio;\n            canvas.getContext('2d').scale(ratio, ratio);\n            this.signaturePad.clear();\n\n            this.signatureData = TRANSPARENT_PNG;\n            this.signaturePad.fromData(data);\n        },\n        saveSignature() {\n            const { signaturePad, saveType } = this;\n            const status = { isEmpty: false, data: undefined };\n\n            if (!checkSaveType(saveType)) {\n                throw new Error('Image type is incorrect!');\n            }\n\n            if (signaturePad.isEmpty()) {\n                return {\n                    ...status,\n                    isEmpty: true\n                };\n            } else {\n                this.signatureData = signaturePad.toDataURL(saveType);\n                return {\n                    ...status,\n                    data: this.signatureData,\n                };\n            }\n        },\n        undoSignature() {\n            const { signaturePad } = this;\n            const record = signaturePad.toData();\n\n            if (record) {\n                var undone_record = record.slice(0, -1);\n                if(undone_record.length < 1){\n                    this.pad_state = PAD_STATES.EMPTY;\n                }\n                return signaturePad.fromData(undone_record);\n            }\n            else{\n                this.pad_state = PAD_STATES.EMPTY;\n            }\n        },\n        mergeImageAndSignature(customSignature) {\n            this.signatureData = customSignature;\n\n            return mergeImages([\n                ...this.images,\n                ...this.cacheImages,\n                this.signatureData\n            ]);\n        },\n        addImages(images = []) {\n            this.cacheImages = [...this.cacheImages, ...images];\n            return mergeImages([\n                ...this.images,\n                ...this.cacheImages,\n                this.signatureData\n            ]);\n        },\n        fromDataURL(data) {\n            return this.signaturePad.fromDataURL(data);\n        },\n        lockSignaturePad() {\n            return this.signaturePad.off();\n        },\n        openSignaturePad() {\n            return this.signaturePad.on();\n        },\n        isEmpty() {\n            return this.signaturePad.isEmpty();\n        },\n        getPropImagesAndCacheImages() {\n            return this.propsImagesAndCustomImages;\n        },\n        clearCacheImages() {\n            this.cacheImages = [];\n\n            return this.cacheImages;\n        },\n        clearSignature() {\n            this.pad_state = PAD_STATES.EMPTY;\n            return this.signaturePad.clear();\n        }\n    },\n    computed: {\n        propsImagesAndCustomImages() {\n            const nonReactiveProrpImages = convert2NonReactive(this.images);\n            const nonReactiveCachImages = convert2NonReactive(this.cacheImages);\n\n            return [...nonReactiveProrpImages, ...nonReactiveCachImages];\n        }\n    },\n    watch:{\n        pad_state: function(new_val, old_val){\n            this.$emit('state-change', new_val);\n        }\n    }\n};\n</script>\n<style>\n#canvas-container {}\n\n#signature-canvas {\n    width: 100%;\n    height: 100%;\n}\n</style>"]}, media: undefined });

      };
      /* scoped */
      const __vue_scope_id__ = undefined;
      /* module identifier */
      const __vue_module_identifier__ = undefined;
      /* functional template */
      const __vue_is_functional_template__ = false;
      /* component normalizer */
      function __vue_normalize__(
        template, style, script$$1,
        scope, functional, moduleIdentifier,
        createInjector, createInjectorSSR
      ) {
        const component = (typeof script$$1 === 'function' ? script$$1.options : script$$1) || {};

        // For security concerns, we use only base name in production mode.
        component.__file = "/Users/matt.gauthier/Projects/university-of-utah/gitlab/utah-coe-public/vue-signature-pad/src/components/VueSignaturePad.vue";

        if (!component.render) {
          component.render = template.render;
          component.staticRenderFns = template.staticRenderFns;
          component._compiled = true;

          if (functional) component.functional = true;
        }

        component._scopeId = scope;

        {
          let hook;
          if (style) {
            hook = function(context) {
              style.call(this, createInjector(context));
            };
          }

          if (hook !== undefined) {
            if (component.functional) {
              // register for functional component in vue file
              const originalRender = component.render;
              component.render = function renderWithStyleInjection(h, context) {
                hook.call(context);
                return originalRender(h, context)
              };
            } else {
              // inject component registration as beforeCreate hook
              const existing = component.beforeCreate;
              component.beforeCreate = existing ? [].concat(existing, hook) : [hook];
            }
          }
        }

        return component
      }
      /* style inject */
      function __vue_create_injector__() {
        const head = document.head || document.getElementsByTagName('head')[0];
        const styles = __vue_create_injector__.styles || (__vue_create_injector__.styles = {});
        const isOldIE =
          typeof navigator !== 'undefined' &&
          /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());

        return function addStyle(id, css) {
          if (document.querySelector('style[data-vue-ssr-id~="' + id + '"]')) return // SSR styles are present.

          const group = isOldIE ? css.media || 'default' : id;
          const style = styles[group] || (styles[group] = { ids: [], parts: [], element: undefined });

          if (!style.ids.includes(id)) {
            let code = css.source;
            let index = style.ids.length;

            style.ids.push(id);

            if (isOldIE) {
              style.element = style.element || document.querySelector('style[data-group=' + group + ']');
            }

            if (!style.element) {
              const el = style.element = document.createElement('style');
              el.type = 'text/css';

              if (css.media) el.setAttribute('media', css.media);
              if (isOldIE) {
                el.setAttribute('data-group', group);
                el.setAttribute('data-next-index', '0');
              }

              head.appendChild(el);
            }

            if (isOldIE) {
              index = parseInt(style.element.getAttribute('data-next-index'));
              style.element.setAttribute('data-next-index', index + 1);
            }

            if (style.element.styleSheet) {
              style.parts.push(code);
              style.element.styleSheet.cssText = style.parts
                .filter(Boolean)
                .join('\n');
            } else {
              const textNode = document.createTextNode(code);
              const nodes = style.element.childNodes;
              if (nodes[index]) style.element.removeChild(nodes[index]);
              if (nodes.length) style.element.insertBefore(textNode, nodes[index]);
              else style.element.appendChild(textNode);
            }
          }
        }
      }
      /* style inject SSR */
      

      
      var VueSignaturePad = __vue_normalize__(
        { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
        __vue_inject_styles__,
        __vue_script__,
        __vue_scope_id__,
        __vue_is_functional_template__,
        __vue_module_identifier__,
        __vue_create_injector__,
        undefined
      );

    return VueSignaturePad;

})));
